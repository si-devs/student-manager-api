<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Manager;
use App\Student;
use App\Page;
use App\System;
use App\Tcc;

class ManagerController extends Controller
{

    public function __construct()
    {
        
    }

    public function login(Request $request) {
      $data = $request->only('login', 'authentication');

      $validate = [
        'login' => 'required',
        'authentication' => 'required'
      ];

      $this->validate($request, $validate);
      
      $manager = Manager::where('login', $data['login'])->first();

      if($manager) {
        if($manager->authentication == $data['authentication'] && $manager->login == $data['login']) {
          $manager->manager_token = str_random(60);
          $manager->update();
          return ['manager_token' => $manager->manager_token, 'login' => $manager->login, 'id_manager' => $manager->id_manager ];
        } else {
          return new Response( ['invalid' => 'Login ou senha invalido'] , 401);
        }
      }

      return new Response( ['invalid' => 'Login ou senha invalido'] , 401);
    }

    public function auth() {
      return ['status' => 'ok'];
    }  

    public function uploadCsv(Request $request) {
      $this->validate($request, [
        'file' => 'required|mimes:csv,txt|max:5000'
      ]);

      $upload = $request->file->storeAs('manager', 'insert-class.csv');
      if($upload) {
        $csv = Storage::get($upload);
        $students = $this->prepareStudent($csv);
        $insertClass = $this->insertClass($students);
        Storage::delete($upload);
        if ($insertClass) {
          return new Response( ['send' => 'Turma inserida com sucesso.'] , 200);
        } else {
          return new Response( ['invalid' => 'Oops! houve um erro na inserção da turma, tente novamente mais tarde :('] , 401);
        }
      } else {
        return new Response( ['fail' => 'Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :('] , 401);
      }
    }
  
    public function prepareStudent($csv) {
      $students = explode("\n", $csv);
      foreach ($students as $value) { $student[] = explode(";", $value);  }
      array_shift($student);
      array_pop($student);    
      return $student;
    }
  
    public function insertClass($students) {         
      try {
        DB::transaction(function() use ($students){ 
          foreach ($students as $value) { 
            $dataStudents = $this-> dataStudents($value[0]);      
            $student = $this->insertStudent($value,$dataStudents);
            $this->insertPage($student, $dataStudents['page_link']);
            $this->insertSystem($student);
            $this->insertTcc($student,$value,$dataStudents);
          }
        });
      }
      catch(\Exception $e){
        return false;
      }
      return true;
    }

    public function insertStudent($value,$dataStudents) {
      $student = new Student();
      $student->ra = $value[0];
      $student->name = $value[1];
      $student->authentication = Crypt::encrypt(str_random(6));
      $student->year_entrance = $dataStudents['year_entrance'];
      $student->id_course = $value[3];
      $student->student_token = str_random(60);
      $student->save();
      return $student;
    }
  
    public function insertPage($student, $page_link) {
      $page = new Page();
      $page->page_link = $page_link;
      $page->id_student = $student->id_student;
      $page->save();
      return $page;
    }
  
    public function insertSystem($student) {
      $system = new System();
      $system->id_student = $student->id_student;
      $system->system_link = 'Não enviado';
      $system->save();
      return $system;
    }

    public function insertTcc($student, $value, $dataStudents) {
      $tcc = new Tcc();
      $tcc->title = $value[4];
      $tcc->publish = $value[5];
      $tcc->send = $value[6];
      $tcc->approval = $value[7];
      $tcc->approval_link = $dataStudents['approval_link'];
      $tcc->tcc_link = $dataStudents['tcc_link'];
      $tcc->id_student = $student->id_student;
      $tcc->save();
      return $tcc;
    }

    public function dataStudents($ra) {
      $year_entrance = '20'.substr($ra, 6, 2);
      return [
        'year_entrance' => $year_entrance,
        'page_link' => 'http://201.55.33.89/si/'.$year_entrance.'/'.$ra.'/index.html',
        'tcc_link' => 'storage/students/'.$ra.'/tcc'.'/'.$ra.'.pdf',
        'approval_link' => 'storage/students/'.$ra.'/approval'.'/'.$ra.'.pdf',
      ];
    }

    public function listStudents($id = null) {
      
      if ($id) {
        $student = Student::join('system', 'system.id_student', '=', 'students.id_student')
        ->join('page', 'page.id_student', '=', 'students.id_student')
        ->join('tcc', 'tcc.id_student', '=', 'students.id_student')
        ->join('courses', 'courses.id_course', '=', 'students.id_course')
        ->select('ra', 'name', 'authentication', 'description',
                 'year_entrance', 'page_link', 'title', 'publish', 'send', 'approval',
                 'system_link', 'tcc_link', 'approval_link', 'students.id_student')
        ->where('students.id_student', '=', $id)->first();

        if ($student) {
          $student->authentication = Crypt::decrypt($student->authentication); 
          return $student;
        } 
        else return new Response( ['fail' => 'Estudante não encontrado '] , 401);
      } 

      $students =  Student::join('system', 'system.id_student', '=', 'students.id_student')
      ->join('page', 'page.id_student', '=', 'students.id_student')
      ->join('tcc', 'tcc.id_student', '=', 'students.id_student')
      ->join('courses', 'courses.id_course', '=', 'students.id_course')
      ->get();

      $student = array();

      foreach ($students as $key => $value){
        array_push($student,  [ 
          $value->ra, 
          $value->name, 
          $value->description,
          $value->year_entrance,
          [ 
            $value->page_link,
            $value->system_link,
            $value->tcc_link,
            $value->approval_link,
          ],
          [
            $value->id_student,
            $value->id_student
          ]
        ]);
      }

      return $student;
    }

    public function listStudentsApproval($id = null) {
      if ($id) {
        $student = Student::join('system', 'system.id_student', '=', 'students.id_student')
        ->join('page', 'page.id_student', '=', 'students.id_student')
        ->join('tcc', 'tcc.id_student', '=', 'students.id_student')
        ->join('courses', 'courses.id_course', '=', 'students.id_course')
        ->select('students.id_student', 'ra', 'name', 'authentication', 'description',
                 'year_entrance', 'page_link',
                 'system_link', 'tcc_link', 'approval_link')
        ->where('students.id_student', '=', $id)->first();

        if ($student) {
          $student->authentication = Crypt::decrypt($student->authentication); 
          return $student;
        } 
        else return new Response( ['fail' => 'Estudante não encontrado '] , 401);
      } 

      $students = Student::join('system', 'system.id_student', '=', 'students.id_student')
      ->join('page', 'page.id_student', '=', 'students.id_student')
      ->join('tcc', 'tcc.id_student', '=', 'students.id_student')
      ->join('courses', 'courses.id_course', '=', 'students.id_course')
      ->where('tcc.send', '=', '1')
      ->where('tcc.approval', '=', '0')
      ->get();

      $student = array();

      foreach ($students as $key => $value){
        array_push($student,  [ 
          $value->ra, 
          $value->name, 
          $value->description,
          $value->year_entrance,
          [ 
            $value->page_link,
            $value->system_link,
            $value->tcc_link,
            $value->approval_link
          ],
          $value->id_student
        ]);
      }

      return $student;

    }

    public function updateStudent(Request $request, $id) {
      
      $data = $request->only('ra', 'name', 'authentication', 'page_link', 'system_link');

      $validate = [
        'ra' => 'required',
        'name' => 'required',
        'authentication' => 'required',
        'page_link' => 'required',
        'system_link' => 'required',
      ];
      
      $this->validate($request, $validate);

      $student = Student::where('ra', '=', $data['ra'])->where('id_student', '=', $id)->first();      
      $page = Page::where('page.id_student','=',$id)->first();
      $system = System::where('system.id_student','=',$id)->first();

      if ($student && $page && $system) {
        $student->name = $data['name'];
        $student->authentication = Crypt::encrypt($data['authentication']);
        $page->page_link = $data['page_link'];
        $system->system_link = $data['system_link'];
        $student->update();
        $page->update();
        $system->update();
        return new Response( ['success' => 'Estudante atualizado com sucesso. '] , 200);
      } else return new Response( ['fail' => 'Erro ao atualizar estudante.'] , 401);

    }
}
