<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Tcc;
use App\System;

class TccController extends Controller
{

    public function __construct()
    {
        
    }

    public function view_send() {
      return ['status' => 'ok'];
    }

    public function send(Request $request, $id) {
      $data = $request->only('file', 'title','year','link_system');

      $this->validate($request, [
        'file' => 'required|mimes:pdf|max:5000',
        'title' => 'required',
        'year' => 'required|date_format:"Y"',
        'link_system' => 'required'
      ]);

      $tcc = Tcc::join('students', 'tcc.id_student', '=', 'students.id_student')
              ->where('tcc.id_student', '=', $id)
              ->first();

      $system = System::join('students', 'system.id_student', '=', 'students.id_student')
              ->where('system.id_student', '=', $id)
              ->first();

      if (!$tcc->send) {
        $upload = $request->file->storeAs('public/students/'.$tcc->ra.'\/tcc/', $tcc->ra.'.pdf');
        if($upload) {
          $tcc->title = $data['title'];
          $tcc->publish = $data['year'];
          $tcc->send = 1;
          $tcc->update();

          $system->system_link = $data['link_system'];
          $system->update();

          return new Response( ['send' => 'TCC enviado para a aprovação.'] , 200);
        } else {
          return new Response( ['fail' => 'Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :('] , 401);
        }
      } else {
        return new Response( ['invalid' => 'Você já solicitou a aprovação do TCC, caso tenha enviado errado, contate o administrador!'] , 401);
      }
    }

    public function update(Request $request, $id) {

      $data = $request->only('title', 'publish', 'send', 'approval','tcc_link', 'approval_link', 'file', 'fileApproval');

      $validate = [
        'send' => 'required',
        'approval' => 'required',
        'publish' => 'date_format:"Y"',
        'file' => 'mimes:pdf',
        'fileApproval' =>'mimes:pdf',
      ];

      $this->validate($request, $validate);

      $tcc = Tcc::join('students', 'tcc.id_student', '=', 'students.id_student')
      ->where('tcc.id_student', '=', $id)
      ->first();

      if($tcc) {
        $tcc->title = $data['title'];
        $tcc->publish = $data['publish'];
        $tcc->send = $data['send'];
        $tcc->approval = $data['approval'];
        $tcc->tcc_link = $data['tcc_link'];
        $tcc->approval_link = $data['approval_link'];

        if ($request->file('file')) {
          $upload = $request->file->storeAs('public/students/'.$tcc->ra.'\/tcc/', $tcc->ra.'.pdf');
          $tcc->send = 1;
          if (!$upload) {
            return new Response( ['fail' => 'Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :('] , 401);
          }
        }

        if ($request->file('fileApproval')) {
          $upload = $request->fileApproval->storeAs('public/students/'.$tcc->ra.'\/approval/', $tcc->ra.'.pdf');
          $tcc->approval = 1;
          if (!$upload) {
            return new Response( ['fail' => 'Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :('] , 401);
          }
        }

        $tcc->update();
        return new Response( ['success' => 'Tcc do estudante atualizado com sucesso .'] , 200);
      } else return new Response( ['fail' => 'Erro ao atualizar tcc do estudante .'] , 401);
      
    }

    public function approval(Request $request, $id) {
      $data = $request->only('file');

      $this->validate($request, [
        'file' => 'required|mimes:pdf|max:5000',
      ]);

      $tcc = Tcc::join('students', 'tcc.id_student', '=', 'students.id_student')
              ->where('tcc.id_student', '=', $id)
              ->first();

      if ($tcc->send === 1) {
        $upload = $request->file->storeAs('public/students/'.$tcc->ra.'\/approval/', $tcc->ra.'.pdf');
        if($upload) {
          $tcc->approval = 1;
          $tcc->update();
          return new Response( ['send' => 'TCC aprovado com sucesso.'] , 200);
        } else {
          return new Response( ['fail' => 'Não foi possivel realizar o upload do arquivo, tente novamente mais tarde :('] , 401);
        }
      } else {
        return new Response( ['invalid' => 'Não foi possivel realizar a aprovação do TCC.'] , 401);
      }
    }
}