<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Student;

class StudentController extends Controller
{

    public function __construct()
    {
        
    }

    
  public function login(Request $request) {
      $data = $request->only('ra', 'authentication');

      $validate = [
        'ra' => 'required',
        'authentication' => 'required'
      ];

      $this->validate($request, $validate);
      
      $student = Student::where('ra', $data['ra'])->first();

      if($student) {
  
        if(Crypt::decrypt($student->authentication) == $data['authentication'] && $student->ra == $data['ra']) {
          $student->student_token = str_random(60);
          $student->update();
          return ['student_token' => $student->student_token, 'ra' => $student->ra, 'id_student' => $student->id_student ];
        } else {
          return new Response( ['invalid' => 'Login ou senha invalido'] , 401);
        }
      }

      return new Response( ['invalid' => 'Login ou senha invalido'] , 401);
    }

    public function auth() {
      return ['status' => 'ok'];
    }
  
   public function update(Request $request, $id) {

    $data = $request->only('authentication', 'authentication_confirmation', 'old_authentication');

    $validate = [
      'authentication' => 'required|confirmed|max: 255',
      'old_authentication' => 'required|max: 255',
    ];

    $this->validate($request, $validate);

    $student = Student::find($id);

    if (Crypt::decrypt($student->authentication) == $data['old_authentication']) {
      $student->authentication = Crypt::encrypt($request->input('authentication'));
      $student->update();
      return $student;
    } else {
      return new Response( ['invalid' => 'Senha atual não confere! :('] , 401);
    }

  }
}