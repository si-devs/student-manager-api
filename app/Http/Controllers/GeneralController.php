<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use App\Student;

class GeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function list()
    {
      $students = Student::join('system', 'system.id_student', '=', 'students.id_student')
      ->join('page', 'page.id_student', '=', 'students.id_student')
      ->join('tcc', 'tcc.id_student', '=', 'students.id_student')
      ->join('courses', 'courses.id_course', '=', 'students.id_course')
      ->get();

      // return $students;

      $student = array();

      foreach ($students as $key => $value){
        array_push($student,  [ 
          "RA" => $value->ra, 
          "NOME" => $value->name, 
          "CURSO" => $value->description,
          "INGRESSO" => $value->year_entrance,
          "DOCUMENTOS" => [
            $value->page_link,
            $value->system_link,
            $value->tcc_link,
            $value->approval_link
          ]
        ]);
      }

      return $student;
    }
}
