<?php

namespace App\Providers;

use App\Student;
use App\Manager;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Filesystem\Factory;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            $student_token = '';
            $manager_token = '';

            if ($request->header('student_token')) {
                if ($request->header('student_token') && $request->header('ra') ) {
                    $student_token = $request->header('student_token');
                    $ra = $request->header('ra');
                } else {
                    $student_token = $request->input('student_token');
                    $ra = $request->input('ra');
                }
                return Student::where('student_token', $student_token)
                                ->where('ra', $ra)
                                ->first();
            }

            if ($request->header('manager_token')) {
                
                if ($request->header('manager_token') && $request->header('login') ) {
                    $manager_token = $request->header('manager_token');
                    $login = $request->header('login');
                } else {
                    $manager_token = $request->input('manager_token');
                    $login = $request->input('login');
                }
                return Manager::where('manager_token', $manager_token)
                                ->where('login', $login)
                                ->first();
            }
        
          });
    }
}
