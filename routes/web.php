<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/home', 'GeneralController@list');
$router->post('/student', 'StudentController@login');
$router->post('/manager', 'ManagerController@login');

$router->group(['prefix' => 'student', 'middleware' => 'auth'], function ($router)
{
  $router->get('/auth', 'StudentController@auth');
  $router->post('/send_tcc/{id}', 'tccController@send');  
  $router->put('/profile/{id}', 'StudentController@update');
});

$router->group(['prefix' => 'manager', 'middleware' => 'auth'], function ($router)
{
  $router->get('/auth', 'ManagerController@auth');  
  $router->get('/list_students[/{id}]', 'ManagerController@listStudents');
  $router->get('/list_students_approval[/{id}]', 'ManagerController@listStudentsApproval');
  $router->post('/insert_class', 'ManagerController@uploadCsv');
  $router->put('/list_students/{id}', 'ManagerController@updateStudent');  
  $router->post('/approval_tcc/{id}', 'TccController@approval');
  $router->post('/update_tcc/{id}', 'tccController@update');
});

