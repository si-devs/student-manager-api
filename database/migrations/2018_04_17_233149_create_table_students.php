<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('students', function (Blueprint $table) {
          $table->increments('id_student');
          $table->char('ra', 15);
          $table->string('name', 100);
          $table->string('authentication', 255);
          $table->year('year_entrance', 4);
          $table->integer('id_course')->unsigned();
          $table->foreign('id_course')->references('id_course')->on('courses');
             
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
