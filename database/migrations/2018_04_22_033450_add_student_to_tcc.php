<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStudentToTcc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tcc', function (Blueprint $table) {
            $table->integer('id_student')->unsigned();
            $table->foreign('id_student')->references('id_student')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tcc', function (Blueprint $table) {
            $table->dropColumn('id_student');
        });
    }
}
