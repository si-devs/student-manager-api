<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTcc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tcc', function (Blueprint $table) {
            $table->increments('id_tcc');
            $table->string('title', 100)->nullable($value = true);
            $table->year('publish')->nullable($value = true);
            $table->boolean('send')->nullable($value = true)->default(0);
            $table->boolean('approval')->nullable($value = true)->default(0);
            $table->string('approval_link', 100)->nullable($value = true);
            $table->string('tcc_link', 100)->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tcc');
    }
}
